

export default {
  apiUrl: process.env.API_URL || 'http://localhost:5000', 
  imgBase: process.env.IMG_BASE || 'http://localhost:5000'
}