import React from "react";
import "./Post.css";
import Avatar from "@material-ui/core/Avatar";
import envConfig from "./config/env.config";

function Post(props) {
  return (
    <div className="post">
      <div className="post__header">
        <Avatar className="post__avatar" alt="vaishnav" src="" />

        <h3>{ props.postData.author.username}</h3>
      </div>

      <img
        className="post__image "
        src={
          `${envConfig.imgBase}${props.postData.media.url}` ||
          "https://1.bp.blogspot.com/-10xVVy8GzRQ/Wmss4HgCKVI/AAAAAAAAQZ8/Khed9ub9A90534IjmQQ1Xf-yavObZMRMgCLcBGAs/s1600/oddi-chocolaty-golden-color-pics-walls.jpg"
        }
      ></img>

      <h4 className="post__text">
        <strong>gdfhgd</strong> Username: caption
      </h4>
    </div>
  );
}

export default Post;
