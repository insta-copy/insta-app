import Cookies from "js-cookie";

export default {
  getToken() {
    return Cookies.get("_token");
  },

  setToken(token) {
    Cookies.set("_token", token);
  },

  // setUser(user){

  // },
  removeToken() {
    Cookies.remove("_token");
  },
};
