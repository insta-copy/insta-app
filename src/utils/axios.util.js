import axios from "axios";
import envConfig from "../config/env.config";
import tokenUtil from "./token.util";

export default {
  createInstance() {
    let token = tokenUtil.getToken();
    let options = {
      baseUrl: envConfig.apiUrl,
    };
    // if (token) {
    //   options.headers = { Authorization: `Bearer ${token}` };
    // }

    const instance = axios.create(options);
    return instance
  },
};
