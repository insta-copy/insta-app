import React from "react";

import Home from "./components/auth/Home";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import {
  BrowserRouter,
  Switch,
  useLocation,
  Route,
  Link,Redirect
} from "react-router-dom";
import UserPosts from "./components/user/UserPosts";

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

function App() {
  return (
    <div>

      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>{" "}
          <Route path="/users">
            <UserPosts/>  
          </Route>
          <Route path="*">
            <NoMatch />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
