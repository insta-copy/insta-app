import React, { useState, useEffect } from "react";
import "./Home.css";
import Post from "../../Post";
import postService from "../../services/post.service";
import tokenUtil from "../../utils/token.util";
import Login from "./Login";
import { Redirect, Route } from "react-router";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import axios from "axios";
import authService from "../../services/auth.service";
import UserPosts from "../user/UserPosts";
// import { Button } from "@material-ui/core";
// import SimpleModal from "./components/auth/SimpleModal";

function Home() {
  const [posts, setPosts] = useState([]);
  const [loggedIn, setLoggedIn] = useState(false);
  const [showLoginForm, setShowLoginForm] = useState(false);

  useEffect(() => {
    if (!loggedIn) {
      let token = tokenUtil.getToken();
      if (token) {
        setLoggedIn(true);
      }
    }
    if (!posts || !posts.length) {
      postService
        .getAllPosts()
        .then((response) => {
          setPosts(response.data);
        })
        .catch((err) => alert(err.message));
    }
  }, []);

  // onClick=()=>{
  //   <Route exact path="/userposts">
  //   {loggedIn ? <Redirect to="/userposts" /> : <UserPosts />}
  // </Route>
  
  // }

  // const userDetails = (e) => {
  //   authService.userDetails
  //   .then((res) => console.log(res))
  //   .catch((err) => console.log(err))

  //   return (
  //     <div>

  //     </div>
  //   )
  // };
  return (
    <div className="App">
      {/* <SimpleModal/> */}
      <div className="app__header">
        {/* <img className="app__headerImage"
        src="https://1000logos.net/wp-content/uploads/2017/02/Instagram-Logo-500x313.png"
        alt="Instgram"
        /> */}
        <div className="app__headerImage"> Instagram</div>
        {/* <Link to="/register">Register</Link> */}
        <Button component={Link} to="/register">
          Register
        </Button>

        {loggedIn ? (
          <button
            onClick={() => {
              setShowLoginForm(false);
              tokenUtil.removeToken();
              setLoggedIn(false);
            }}
          >
            Log Out
          </button>
        ) : (
          <button onClick={() => setShowLoginForm(!showLoginForm)}>
            Login
          </button>
        )}

        {/* <Button component={Redirect} to="/register">
          User
        </Button> */}
      </div>
      {showLoginForm && !loggedIn ? (
        <div>
          <Login updateState={(val) => setLoggedIn(val)}></Login>
        </div>
      ) : null}
        <button >UserPosts</button>

      <div
        className="container"
        style={{ display: "flex", justifyContent: "center" }}
      >
        <div className="post-container">
          {posts.map((post) => (
            <Post postData={post} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Home;
