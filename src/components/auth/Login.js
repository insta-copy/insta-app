import { useState } from "react";
import authService from "../../services/auth.service";
import tokenUtil from "../../utils/token.util";

function Login(props) {
  const [identifier, setIdentifier] = useState("");
  const [password, setPassword] = useState("");

  const logIn = (e) => {
    if (e) {
      e.preventDefault();
    }
    if (identifier && password) {
      authService
        .logIn({ identifier, password })
        .then((res) => {
          tokenUtil.setToken(res.data.user);
          props.updateState(true)
        })
        .catch((err) => {alert(err.message); props.updateState(false)});
    }
  };

  return (
    <div>
      <form onSubmit={logIn}>
        <input
          type="text"
          placeholder="Email or Username"
          onChange={(e) => setIdentifier(e.target.value)}
        ></input>

        <input
          type="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        ></input>

        <button type="submit" onClick={logIn}>
          Log In
        </button>
      </form>
    </div>
  );
}

export default Login;
