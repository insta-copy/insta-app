import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import authService from "../../services/auth.service";
import { Button, Input } from "@material-ui/core";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SimpleModal() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const [username, setUsername] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");

  // const handleOpen = () => {
  //   setOpen(true);
  // };

  const handleClose = () => {
    setOpen(false);
  };

  const register = (e) => {
    if (e) {
      e.preventDefault();
    }
    if (username && password) {
      authService
        .register({username,email,password})
        .then((res) => {
         console.log(res);
        })
        .catch((err) => {alert(err.message)});
    }else{
      alert("Enter correct details")
    }
  };
 
  return (
    <div>
      {/* <button type="button" onClick={handleOpen}>
        Sign up
      </button> */}
      <Modal open="true" onClose={handleClose}>
        <div style={modalStyle} className={classes.paper}>
          <form style={{ display: "flex", flexDirection: "column" }} onSubmit={register}>
            <center>
              {/* <img /> */}
              <h2>Instagram</h2>
            </center>
            <Input
              type="text"
              placeholder="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <Input
              type="email"
              placeholder="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Input
              type="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button onClick={register}>Register</Button>
          </form>
        </div>
      </Modal>
    </div>
  );
}
