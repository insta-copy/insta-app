import axiosUtil from "../utils/axios.util";
import jwtDecode from "jwt-decode";

import axios from "axios";
import tokenUtil from "../utils/token.util";
import envConfig from "../config/env.config";
// const $axios = axiosUtil.createInstance();
// import jwtDecode from "jwt-decode";

export default {
  getAllPosts() {
    return axios.get(`http://localhost:5000/posts`);
  },

  // getMyPosts() {
  //   let token = tokenUtil.getToken();
  //   if (token) {
  //     let payload = jwtDecode(token);
  //     if (payload) {
  //       return axios.get(`${envConfig.apiUrl}/posts?author.id=${payload.id}`);
  //     }
  //   }

  //   throw new Error('Unauthorized User')
  // },
};
