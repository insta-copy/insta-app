import axios from "axios";
import envConfig from "../config/env.config";

export default {
  logIn(loginData) {
    return axios.post(`${envConfig.apiUrl}/auth/local`, loginData);
  },
  register(registerData){
    return axios.post(`${envConfig.apiUrl}/auth/local/register`,registerData)
  },
  userDetails(){
    return axios.get(`${envConfig.apiUrl}/users`)
  }
};
